package com.paic.arch.jmsbroker;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

import org.springframework.util.StringUtils;

public class JmsMessageManage {
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";


	private  JmsMessage msg;
	
	public JmsMessageManage(JmsMessage msg){
		this.msg = msg;
	}

    public static JmsMessageManage bindToBrokerAtUrl(String aBrokerUrl,String name) throws Exception {
    	JmsMessage msg = new JmsMessage();
    	if(StringUtils.isEmpty(aBrokerUrl)){
    		msg.setBrokerURL(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    	}else{
    		msg.setBrokerURL(aBrokerUrl);
    	}
    	
    	msg.setName(name);
    	return new JmsMessageManage(msg);
    }
  
    public JmsMessageManage sendMessage(final String message){
    	msg.sendMessage(message,false);
    	return this;
    }
    public String reciveMessage(long timeout){
    	return msg.reciveMessage(timeout,false);
    }
    
    public long getEnqueuedMessageCountAt() throws Exception{
    	return msg.getEnqueuedMessageCountAt();
    }
    
    public JmsMessageManage init(){
    	msg.init();
    	return this;
    }
    
	public JmsMessageManage close(){
		msg.close();
		return this;
	}
	
	public JmsMessageManage createARunningEmbeddedBrokerAt() throws Exception{
		msg.createARunningEmbeddedBrokerAt();
		return this;
	}
	
	public JmsMessageManage stopTheRunningBroker() throws Exception{
		msg.stopTheRunningBroker();
		return this;
	}
	
	
}
