package com.paic.arch.jmsbroker;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static JmsMessageManage JMS_SUPPORT;
	private static final int ONE_SECOND = 1000;
	private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;

    @BeforeClass
    public static void setup() throws Exception {
    	JMS_SUPPORT = JmsMessageManage.bindToBrokerAtUrl(null,TEST_QUEUE);
    	JMS_SUPPORT.createARunningEmbeddedBrokerAt();
    }

    @AfterClass
    public static void teardown() throws Exception {
        JMS_SUPPORT.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
    	JMS_SUPPORT.init().sendMessage(MESSAGE_CONTENT).reciveMessage(DEFAULT_RECEIVE_TIMEOUT);
        long messageCount = JMS_SUPPORT.getEnqueuedMessageCountAt();
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = JMS_SUPPORT.init().sendMessage(MESSAGE_CONTENT).reciveMessage(DEFAULT_RECEIVE_TIMEOUT);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }

    @Test(expected = JmsMessage.NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
    	JMS_SUPPORT.init().reciveMessage(1);
    }


}