package com.paic.arch.jmsbroker;
import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;


public class JmsMessage {
	private static final Logger LOG = getLogger(JmsMessage.class);
	 //连接账号
    protected String userName = "";
    //连接密码
    protected String password = "";
    //连接地址
    protected String brokerURL = "tcp://localhost:";
    //connection的工厂
    protected ConnectionFactory factory;
    //连接对象
    protected Connection connection;
    //一个操作会话
    protected Session session;
    //目的地，其实就是连接到哪个队列，如果是点对点，那么它的实现是Queue，如果是订阅模式，那它的实现是Topic
    protected Destination destination;
    //生产者，就是产生数据的对象
    private MessageProducer producer;
    
    //消费者，就是接收数据的对象
    private MessageConsumer consumer;
    
	private BrokerService brokerService;
	
	private String name;
    
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBrokerURL() {
		return brokerURL;
	}
	public void setBrokerURL(String brokerURL) {
		this.brokerURL = brokerURL;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void  createARunningEmbeddedBrokerAt() throws Exception {
	       LOG.debug("Creating a new broker at {}", brokerURL);
	       createEmbeddedBroker();
	       startEmbeddedBroker();
	    }

	    private void createEmbeddedBroker() throws Exception {
	        brokerService = new BrokerService();
	        brokerService.setPersistent(false);
	        brokerService.addConnector(brokerURL);
	    }

	    private void startEmbeddedBroker() throws Exception {
	        brokerService.start();
	    }

	    public void stopTheRunningBroker() throws Exception {
	        if (brokerService == null) {
	            throw new IllegalStateException("Cannot stop the broker from this API: " +
	                    "perhaps it was started independently from this utility");
	        }
	        brokerService.stop();
	        brokerService.waitUntilStopped();
	    }
	
	public void init(){
		try {
	        factory = new ActiveMQConnectionFactory(brokerURL);
	        //从工厂中获取一个连接
	        connection = factory.createConnection();
	        
	        connection.start();
		} catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", getBrokerURL());
            throw new IllegalStateException(jmse);
        }   
	        try {
				session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			} catch (JMSException jmse) {
	            LOG.error("failed to create session to {}", getBrokerURL());
	            throw new IllegalStateException(jmse);
	        }  
		
	}
	public void close() {
		// 关闭连接
			if (connection != null) {
			
				try {
					connection.close();
				} catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", brokerURL);
                    throw new IllegalStateException(jmse);
                }
			
			}		
			// 关闭session
			if (session != null) {
				try {
					session.close();
				} catch (JMSException jmse) {
					LOG.warn("Failed to close session {}", session);
		            throw new IllegalStateException(jmse);
		        } 
			}
	}
	
	public void sendMessage(final String message,boolean flag) {
	try {
		if(flag){
			destination= session.createTopic(name);
		}else{
			// 创建队列
			destination= session.createQueue(name);
		}
		

		// 创建消息生产者
		producer = session.createProducer(destination);

		// 发送消息
		producer.send(session.createTextMessage(message));

		// 关闭生产者
		producer.close();

	} catch (JMSException e) {
		
		LOG.error("failed to send message!");

		throw new IllegalStateException(e);
		
	} 
	}
   
	public String reciveMessage(long timeOut,boolean flag){
		String reciveMessage = null;
				
				try {
					if(flag){
						destination= session.createTopic(name);
					}else{
						// 创建队列
						destination= session.createQueue(name);
					}
					
				   consumer = session.createConsumer(destination);
					
					//接收消息
					Message message = consumer.receive(timeOut);
					
					//关闭消费者
					consumer.close();
					
					if (message == null) {
						
		                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", timeOut));
		                
		            }
					
					reciveMessage = ((TextMessage) message).getText();
					
				} catch (JMSException e) {
					
					LOG.error("failed to retrieve message!");
		
					throw new IllegalStateException(e);
				}finally {
					close();
				}
				return reciveMessage;
				
	}
	 public long getEnqueuedMessageCountAt() throws Exception {
	        return getDestinationStatisticsFor().getMessages().getCount();
	    }

	    public boolean isEmptyQueueAt() throws Exception {
	        return getEnqueuedMessageCountAt() == 0;
	    }
	    
	    private DestinationStatistics getDestinationStatisticsFor() throws Exception {
	        Broker regionBroker = brokerService.getRegionBroker();
	        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
	            if (destination.getName().equals(name)) {
	                return destination.getDestinationStatistics();
	            }
	        }
	        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", name, brokerURL));
	    }
	
	 public class NoMessageReceivedException extends RuntimeException {
	        public NoMessageReceivedException(String reason) {
	            super(reason);
	        }
	    }
}
